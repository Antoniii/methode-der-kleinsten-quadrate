### Функция Leastsq:  
#### Библиотека подфункций, оптимизируемая в scipy, предоставила функцию leastsq, которая реализует алгоритм подбора наименьших квадратов.

![](https://gitlab.com/Antoniii/methode-der-kleinsten-quadrate/-/raw/main/index.png)

## Sources

* [SciPy-библиотека численных расчетов](https://russianblogs.com/article/2229444416/)  
* [Метод наименьших квадратов](https://ru.wikipedia.org/wiki/Метод_наименьших_квадратов)
